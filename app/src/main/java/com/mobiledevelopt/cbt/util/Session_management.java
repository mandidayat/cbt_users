package com.mobiledevelopt.cbt.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.mobiledevelopt.cbt.activity.LoginActivity;
import com.mobiledevelopt.cbt.model.login.M_login_data;

import java.util.HashMap;

import static com.mobiledevelopt.cbt.api.Const.IS_LOGIN;
import static com.mobiledevelopt.cbt.api.Const.KEY_AHLI_WARIS;
import static com.mobiledevelopt.cbt.api.Const.KEY_ALAMAT;
import static com.mobiledevelopt.cbt.api.Const.KEY_AN;
import static com.mobiledevelopt.cbt.api.Const.KEY_BANK;
import static com.mobiledevelopt.cbt.api.Const.KEY_ID;
import static com.mobiledevelopt.cbt.api.Const.KEY_NAME;
import static com.mobiledevelopt.cbt.api.Const.KEY_NIK;
import static com.mobiledevelopt.cbt.api.Const.KEY_NISN;
import static com.mobiledevelopt.cbt.api.Const.KEY_NOHP;
import static com.mobiledevelopt.cbt.api.Const.KEY_NOREk;
import static com.mobiledevelopt.cbt.api.Const.KEY_NPWP;
import static com.mobiledevelopt.cbt.api.Const.KEY_TANGGAL_DAFTAR;
import static com.mobiledevelopt.cbt.api.Const.KEY_USERNAME;
import static com.mobiledevelopt.cbt.api.Const.KEY_profile_complete;
import static com.mobiledevelopt.cbt.api.Const.PREFS_NAME;

public class Session_management {
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    private Context context;

    public Session_management(Context context) {
        this.context = context;
        int PRIVATE_MODE = 0;
        prefs = context.getSharedPreferences(PREFS_NAME, PRIVATE_MODE);
        editor = prefs.edit();
    }

    public void createLoginSession(M_login_data login_data) {

        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_ID, login_data.getIdPeserta());
        editor.putString(KEY_NISN, login_data.getNisn());
        editor.putString(KEY_NAME, login_data.getNama());
        editor.putString(KEY_ALAMAT, login_data.getAlamat());
        editor.putString(KEY_NOHP, login_data.getNo_hp());

        editor.commit();
    }

    public void checkLogin() {

        if (!this.isLoggedIn()) {
            Intent loginsucces = new Intent(context, LoginActivity.class);
            // Closing all the Activities
            loginsucces.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            loginsucces.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(loginsucces);
        }
    }

    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<>();

        user.put(KEY_ID, prefs.getString(KEY_ID, null));
        user.put(KEY_NISN, prefs.getString(KEY_NISN, null));
        user.put(KEY_NAME, prefs.getString(KEY_NAME, null));
        user.put(KEY_ALAMAT, prefs.getString(KEY_ALAMAT, null));
        user.put(KEY_NOHP, prefs.getString(KEY_NOHP, null));
        user.put(KEY_USERNAME, prefs.getString(KEY_USERNAME, null));
        user.put(KEY_NPWP, prefs.getString(KEY_NPWP, null));
        user.put(KEY_AN, prefs.getString(KEY_AN, null));
        user.put(KEY_NOREk, prefs.getString(KEY_NOREk, null));
        user.put(KEY_BANK, prefs.getString(KEY_BANK, null));
        user.put(KEY_NIK, prefs.getString(KEY_NIK, null));
        user.put(KEY_AHLI_WARIS, prefs.getString(KEY_AHLI_WARIS, null));
        user.put(KEY_TANGGAL_DAFTAR, prefs.getString(KEY_TANGGAL_DAFTAR, null));
        user.put(KEY_profile_complete, prefs.getString(KEY_profile_complete, null));

        // return user
        return user;
    }

    public void updateData(M_login_data login_data) {

        editor.putString(KEY_NAME, login_data.getNama());
        editor.putString(KEY_ALAMAT, login_data.getAlamat());
        editor.putString(KEY_NOHP, login_data. getNo_hp());
        editor.apply();
    }

    public void updateSocity(String socity_name, String socity_id) {

        editor.apply();
    }

    public void logoutSession() {
        editor.clear();
        editor.commit();
//        Intent logout = new Intent(context, LoginActivity.class);
//        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        // Add new Flag to start new Activity
//        logout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        context.startActivity(logout);
    }

    public void logoutSessionwithchangepassword() {
        editor.clear();
        editor.commit();

        Intent logout = new Intent(context, LoginActivity.class);
        // Closing all the Activities
        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        logout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(logout);
    }

    public HashMap<String, String> getdatetime() {

        return new HashMap<String, String>();
    }

    public String getID(){
        return prefs.getString(KEY_ID,null);
    }
    // Get Login State

    public boolean isLoggedIn() {
        return prefs.getBoolean(IS_LOGIN, false);
    }

}

