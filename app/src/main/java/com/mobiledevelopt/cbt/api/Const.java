package com.mobiledevelopt.cbt.api;

public class Const {
    public static String BASE_URL = "http://cbt.appendtechindonesia.com/index.php/";
    public static String BASE_URL_Image = "http://cbt.appendtechindonesia.com/uploads/soal/";

    ///sharedpreference
    public static final String PREFS_NAME = "Session";
    public static final String IS_LOGIN = "isLogin";
    public static final String KEY_ID = "user_id";
    public static final String KEY_NISN = "user_nisn";
    public static final String KEY_NAME = "name";
    public static final String KEY_ALAMAT = "user_alamat";
    public static final String KEY_NOHP = "user_nohp";
    public static final String KEY_USERNAME = "user_name";
    public static final String KEY_NPWP = "user_npwp";
    public static final String KEY_NOREk = "user_norek";
    public static final String KEY_AN = "user_an";
    public static final String KEY_BANK = "user_bank";
    public static final String KEY_AHLI_WARIS = "user_ahli_waris";
    public static final String KEY_NIK = "nik";
    public static final String KEY_TANGGAL_DAFTAR = "tanggal_daftar";
    public static final String KEY_profile_complete = "profile_complete";
}
