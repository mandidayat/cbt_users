package com.mobiledevelopt.cbt.api;

import com.mobiledevelopt.cbt.model.M_add_data;
import com.mobiledevelopt.cbt.model.kuis.M_kuis;
import com.mobiledevelopt.cbt.model.login.M_login;
import com.mobiledevelopt.cbt.model.ujian.M_ujian;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {

    /*
    login user
     */
    @FormUrlEncoded
    @POST("login/peserta")
    Call<M_login> userLogin(
            @Field("nisn") String username,
            @Field("password") String password
    );

    @GET("Soal")
    Call<M_kuis> kuis();

    @FormUrlEncoded
    @POST("ujian/listUjian")
    Call<M_ujian> userUjian(
            @Field("nisn") String username
    );

    @FormUrlEncoded
    @POST("ujian/simpanNilai")
    Call<M_add_data> ujianAddNilai(
            @Field("id_ujian") String id_ujian,
            @Field("id_peserta") String id_peserta,
            @Field("nilai") String nilai
    );
}
