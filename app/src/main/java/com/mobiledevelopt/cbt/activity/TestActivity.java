package com.mobiledevelopt.cbt.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mobiledevelopt.cbt.R;
import com.mobiledevelopt.cbt.api.Const;
import com.mobiledevelopt.cbt.model.M_add_data;
import com.mobiledevelopt.cbt.model.kuis.M_kuis;
import com.mobiledevelopt.cbt.model.kuis.M_kuis_data;
import com.mobiledevelopt.cbt.util.M;
import com.mobiledevelopt.cbt.util.RetrofitClient;
import com.mobiledevelopt.cbt.util.Session_management;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;

public class TestActivity extends AppCompatActivity {
    TextView txtNama, txtNo, txtWaktu, txtSoal;
    Button btnPrev, btnSelesai, btnNext;
    RadioGroup rg;
    RadioButton rb1, rb2, rb3, rb4;
    ImageView img;
    EditText inputNama;
    private String waktu_kuis,id_ujian;
    int jawabanYgDiPilih[] = null;
    int jawabanYgBenar[] = null;
    boolean cekPertanyaan = false;
    int urutanPertanyaan = 0;
    List<M_kuis_data> listSoal;
    JSONArray soal = null;
    int jumlahJawabanYgBenar = 0;
    TestActivity.CounterClass mCountDownTimer;
    private ProgressDialog pDialog;
    private static final String TAG_DAFTAR = "daftar_soal";
    private static final String TAG_ID = "soal_id";
    private static final String TAG_SOAL = "soal";
    private static final String TAG_A = "a";
    private static final String TAG_B = "b";
    private static final String TAG_C = "c";
    private static final String TAG_JWB = "jawaban";
    private static final String TAG_GAMBAR = "gambar";
    private Session_management sessionManagement;
    private HashMap<String, String> user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        sessionManagement = new Session_management(TestActivity.this);
        user = sessionManagement.getUserDetails();

        listSoal = new ArrayList<M_kuis_data>();

        img = (ImageView) findViewById(R.id.imageView1);
        txtNama = (TextView) findViewById(R.id.textViewNama);
        txtNo = (TextView) findViewById(R.id.textViewNo);
        txtWaktu = (TextView) findViewById(R.id.textViewWaktu);
        txtSoal = (TextView) findViewById(R.id.textViewSoal);
        btnPrev = (Button) findViewById(R.id.buttonPrev);
        btnSelesai = (Button) findViewById(R.id.buttonSelesai);
        btnNext = (Button) findViewById(R.id.buttonNext);
        rg = (RadioGroup) findViewById(R.id.radioGroup1);
        rb1 = (RadioButton) findViewById(R.id.radio0);
        rb2 = (RadioButton) findViewById(R.id.radio1);
        rb3 = (RadioButton) findViewById(R.id.radio2);
        rb4 = (RadioButton) findViewById(R.id.radio3);

        Intent intent = getIntent();
        waktu_kuis = intent.getStringExtra("waktu");
        id_ujian = intent.getStringExtra("id_ujian");

        get_kuis();

        btnSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aturJawaban_nya();
                // hitung berapa yg benar
                jumlahJawabanYgBenar = 0;
                for (int i = 0; i < jawabanYgBenar.length; i++) {
                    if ((jawabanYgBenar[i] != -1)
                            && (jawabanYgBenar[i] == jawabanYgDiPilih[i]))
                        jumlahJawabanYgBenar++;
                }

                simpan_nilai(jumlahJawabanYgBenar * 10);


            }
        });
        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aturJawaban_nya();
                urutanPertanyaan--;
                if (urutanPertanyaan < 0)
                    urutanPertanyaan = 0;

                tunjukanPertanyaan(urutanPertanyaan, cekPertanyaan);
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aturJawaban_nya();
                urutanPertanyaan++;
                if (urutanPertanyaan >= listSoal.size())
                    urutanPertanyaan = listSoal.size() - 1;

                tunjukanPertanyaan(urutanPertanyaan, cekPertanyaan);
            }
        });

    }

    private void simpan_nilai(int i) {
        Log.e("test",id_ujian +" "+ user.get(Const.KEY_ID) + " "+ String.valueOf(i));
        M.showLoadingDialog(this);
        Call<M_add_data> call = null;
        call = RetrofitClient.getInstance().getApi().ujianAddNilai(id_ujian, user.get(Const.KEY_ID),String.valueOf(i));
        assert call != null;
        call.enqueue(new Callback<M_add_data>() {
            @Override
            public void onResponse(Call<M_add_data> call, retrofit2.Response<M_add_data> response) {
                M.hideLoadingDialog();
                M_add_data loginResponse = response.body();
                assert loginResponse != null;
                if (loginResponse.getMessage().equals("Data Berhasil Disimpan")){
//                    new SweetAlertDialog(TestActivity.this,SweetAlertDialog.SUCCESS_TYPE)
//                            .setTitleText(loginResponse.getMessage())
//                            .show();
//                    deleteData(position);
                    AlertDialog tampilKotakAlert;
                    tampilKotakAlert = new AlertDialog.Builder(TestActivity.this)
                            .create();
                    tampilKotakAlert.setTitle("Hasil Ujian");
                    tampilKotakAlert.setMessage("Kamu Mendapat Nilai " + jumlahJawabanYgBenar * 10);

//                tampilKotakAlert.setButton(AlertDialog.BUTTON_NEUTRAL, "Lagi",
//                        new DialogInterface.OnClickListener() {
//
//                            public void onClick(DialogInterface dialog, int which) {
//                                mCountDownTimer.cancel();
//                                java.util.Arrays.fill(jawabanYgDiPilih, -1);
//                                cekPertanyaan = false;
//                                urutanPertanyaan = 0;
//                                tunjukanPertanyaan(0, cekPertanyaan);
//                            }
//                        });
//
//                tampilKotakAlert.setButton(AlertDialog.BUTTON_POSITIVE, "Priksa",
//                        new DialogInterface.OnClickListener() {
//
//                            public void onClick(DialogInterface dialog, int which) {
//                                mCountDownTimer.cancel();
//                                cekPertanyaan = true;
//                                urutanPertanyaan = 0;
//                                tunjukanPertanyaan(0, cekPertanyaan);
//                            }
//                        });

                    tampilKotakAlert.setButton(AlertDialog.BUTTON_NEGATIVE, "Keluar",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    mCountDownTimer.cancel();
                                    cekPertanyaan = false;
                                   Intent intent = new Intent(TestActivity.this,DataUjianActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                   startActivity(intent);
                                }
                            });

                    tampilKotakAlert.show();
                }else{
                    new SweetAlertDialog(TestActivity.this,SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(loginResponse.getMessage())
                            .show();
                }

            }

            @Override
            public void onFailure(Call<M_add_data> call, Throwable t) {
                M.hideLoadingDialog();
            }
        });
    }

    private void aturJawaban_nya() {
        if (rb1.isChecked())
            jawabanYgDiPilih[urutanPertanyaan] = 1;
        if (rb2.isChecked())
            jawabanYgDiPilih[urutanPertanyaan] = 2;
        if (rb3.isChecked())
            jawabanYgDiPilih[urutanPertanyaan] = 3;
        if (rb4.isChecked())
            jawabanYgDiPilih[urutanPertanyaan] = 4;

        Log.d("", Arrays.toString(jawabanYgDiPilih));
        Log.d("", Arrays.toString(jawabanYgBenar));

    }

    private void tunjukanPertanyaan(int urutan_soal_soal, boolean review) {
        btnSelesai.setEnabled(false);
//        if(urutan_soal_soal == 0)
//            setUpWaktu();

        try {
            rg.clearCheck();
            M_kuis_data soal = new M_kuis_data();
            soal = listSoal.get(urutan_soal_soal);
            if (jawabanYgBenar[urutan_soal_soal] == -1) {
                jawabanYgBenar[urutan_soal_soal] = Integer.parseInt(soal.getJawaban());
            }

            String soalnya = soal.getSoal();
            Log.e("soal", soalnya);
            txtSoal.setText(soalnya);
            rg.check(-1);
            rb1.setTextColor(Color.WHITE);
            rb2.setTextColor(Color.WHITE);
            rb3.setTextColor(Color.WHITE);
            rb4.setTextColor(Color.WHITE);
//            imageLoader.DisplayImage(soal.getGambar(), img);
            if (soal.getGambar() != null){
                img.setVisibility(View.VISIBLE);
                Glide.with(this).load(Const.BASE_URL_Image+soal.getGambar()).into(img);
            }else {
                img.setVisibility(View.GONE);
            }

            rb1.setText(soal.getPil1());
            rb2.setText(soal.getPil2());
            rb3.setText(soal.getPil3());
            rb4.setText(soal.getPil4());

            Log.d("", jawabanYgDiPilih[urutan_soal_soal] + "");
            if (jawabanYgDiPilih[urutan_soal_soal] == 1)
                rg.check(R.id.radio0);
            if (jawabanYgDiPilih[urutan_soal_soal] == 2)
                rg.check(R.id.radio1);
            if (jawabanYgDiPilih[urutan_soal_soal] == 3)
                rg.check(R.id.radio2);

            pasangLabelDanNomorUrut();

            if (urutan_soal_soal == (listSoal.size() - 1)) {
                btnNext.setEnabled(false);
                btnSelesai.setEnabled(true);
            }

            if (urutan_soal_soal == 0)
                btnPrev.setEnabled(false);

            if (urutan_soal_soal > 0)
                btnPrev.setEnabled(true);

            if (urutan_soal_soal < (listSoal.size() - 1))
                btnNext.setEnabled(true);

            if (review) {
                mCountDownTimer.cancel();
                Log.d("priksa", jawabanYgDiPilih[urutan_soal_soal] + ""
                        + jawabanYgBenar[urutan_soal_soal]);
                if (jawabanYgDiPilih[urutan_soal_soal] != jawabanYgBenar[urutan_soal_soal]) {
                    if (jawabanYgDiPilih[urutan_soal_soal] == 1)
                        rb1.setTextColor(Color.RED);
                    if (jawabanYgDiPilih[urutan_soal_soal] == 2)
                        rb2.setTextColor(Color.RED);
                    if (jawabanYgDiPilih[urutan_soal_soal] == 3)
                        rb3.setTextColor(Color.RED);
                    if (jawabanYgDiPilih[urutan_soal_soal] == 4)
                        rb4.setTextColor(Color.RED);
                }
                if (jawabanYgBenar[urutan_soal_soal] == 1)
                    rb1.setTextColor(Color.GREEN);
                if (jawabanYgBenar[urutan_soal_soal] == 2)
                    rb2.setTextColor(Color.GREEN);
                if (jawabanYgBenar[urutan_soal_soal] == 3)
                    rb3.setTextColor(Color.GREEN);
                if (jawabanYgBenar[urutan_soal_soal] == 4)
                    rb4.setTextColor(Color.GREEN);
            }

        } catch (Exception e) {
            Log.e("Log", e.getMessage(), e.getCause());
        }
    }

    private void setUpWaktu() {
        mCountDownTimer = new CounterClass(60000*Integer.parseInt(waktu_kuis), 1000);
        mCountDownTimer.start();
    }

    @SuppressLint("DefaultLocale")
    public class CounterClass extends CountDownTimer {
        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
            String hms = String.format(
                    "%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis)
                            - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
                            .toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis)
                            - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
                            .toMinutes(millis)));
            txtWaktu.setText(hms);
        }

        @Override
        public void onFinish() {
            txtWaktu.setText("OUT OF TIME!");
            Toast.makeText(TestActivity.this, "TIME OUT", Toast.LENGTH_SHORT).show();
            timeOut();
        }

    }

    private void timeOut() {
        aturJawaban_nya();
        // hitung berapa yg benar
        jumlahJawabanYgBenar = 0;
        for (int i = 0; i < jawabanYgBenar.length; i++) {
            if ((jawabanYgBenar[i] != -1)
                    && (jawabanYgBenar[i] == jawabanYgDiPilih[i]))
                jumlahJawabanYgBenar++;
        }

        simpan_nilai(jumlahJawabanYgBenar * 10);

    }

    private void pasangLabelDanNomorUrut() {
        txtNo.setText("No. " + (urutanPertanyaan + 1)+ " dari "
                + listSoal.size() + " soal");
    }

    private void setUpSoal() {
        Collections.shuffle(listSoal);
        tunjukanPertanyaan(0, cekPertanyaan);
    }

    private void get_kuis() {
        M.showLoadingDialog(this);
        Call<M_kuis> call = RetrofitClient.getInstance().getApi().kuis();
        assert call != null;
        call.enqueue(new Callback<M_kuis>() {
            @Override
            public void onResponse(Call<M_kuis> call, retrofit2.Response<M_kuis> response) {
                M.hideLoadingDialog();
                List<M_kuis_data> res_login = response.body().getData();
                Log.e("size kuis", String.valueOf(res_login.size()));
                listSoal = response.body().getData();
                jawabanYgDiPilih = new int[listSoal.size()];
                java.util.Arrays.fill(jawabanYgDiPilih, -1);
                jawabanYgBenar = new int[listSoal.size()];
                java.util.Arrays.fill(jawabanYgBenar, -1);
                setUpSoal();
                setUpWaktu();
            }

            @Override
            public void onFailure(Call<M_kuis> call, Throwable t) {
                M.hideLoadingDialog();
                Log.e("log", t.getMessage());
                Toast.makeText(getApplicationContext(), "Masalah jaringan, coba lagi. ", Toast.LENGTH_SHORT).show();
            }
        });
    }



}