package com.mobiledevelopt.cbt.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.karan.churi.PermissionManager.PermissionManager;
import com.mobiledevelopt.cbt.R;
import com.mobiledevelopt.cbt.model.kuis.M_kuis;
import com.mobiledevelopt.cbt.model.kuis.M_kuis_data;
import com.mobiledevelopt.cbt.util.ImageLoader;
import com.mobiledevelopt.cbt.util.M;
import com.mobiledevelopt.cbt.util.RetrofitClient;
import com.mobiledevelopt.cbt.util.Session_management;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity {

    private Session_management sessionManagement;
    private HashMap<String, String> user;


    public ImageLoader imageLoader;
    private CardView Logout,card_data_users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //cek login
        sessionManagement = new Session_management(MainActivity.this);
        user = sessionManagement.getUserDetails();

        if (!sessionManagement.isLoggedIn()){
            Intent loginsucces = new Intent(this, LoginActivity.class);
            loginsucces.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            loginsucces.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(loginsucces);
        }

        Logout = findViewById(R.id.log_out);
        card_data_users = findViewById(R.id.card_data_users);

        card_data_users.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,DataUjianActivity.class);
                startActivity(intent);
            }
        });

        Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Yakin Ingin Keluar Akun.?")
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                sessionManagement.logoutSession();
                                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .setCancelButton("Batal", new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
    }



//    private void showInputUser() {
//        LayoutInflater mInflater = LayoutInflater.from(this);
//        View v = mInflater.inflate(R.layout.input_user, null);
//        final AlertDialog dialog = new AlertDialog.Builder(this).create();
//        dialog.setView(v);
//        dialog.setTitle("Isikan Nama Anda");
//        dialog.setIcon(R.drawable.ic_launcher);
//        dialog.setCancelable(false);
//        final Button btnOk = (Button) v.findViewById(R.id.btnOk);
//        final EditText inputNama = (EditText) v.findViewById(R.id.inputID);
//        btnOk.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                if (inputNama.getText().toString().equals("")) {
//                    Toast.makeText(getBaseContext(), "Isikan Nama Anda",
//                            Toast.LENGTH_LONG).show();
//                } else {
//                    txtNama.setText(inputNama.getText().toString());
//                    dialog.dismiss();
//                    get_kuis();
//                }
//
//            }
//        });
//
//        dialog.show();
//
//    }










}