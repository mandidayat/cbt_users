package com.mobiledevelopt.cbt.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.karan.churi.PermissionManager.PermissionManager;
import com.mobiledevelopt.cbt.R;
import com.mobiledevelopt.cbt.model.login.M_login;
import com.mobiledevelopt.cbt.model.login.M_login_data;
import com.mobiledevelopt.cbt.model.login.M_login_user;
import com.mobiledevelopt.cbt.util.M;
import com.mobiledevelopt.cbt.util.RetrofitClient;
import com.mobiledevelopt.cbt.util.Session_management;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends AppCompatActivity {
    private TextInputLayout inputUsername, inputPassword;
    private TextInputEditText edUsername, edPassword;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        PermissionManager permission = new PermissionManager() {
            @Override
            public void ifCancelledAndCanRequest(Activity activity) {
                // Do Customized operation if permission is cancelled without checking "Don't ask again"
                // Use super.ifCancelledAndCanRequest(activity); or Don't override this method if not in use
            }

            @Override
            public void ifCancelledAndCannotRequest(Activity activity) {
                // Do Customized operation if permission is cancelled with checking "Don't ask again"
                // Use super.ifCancelledAndCannotRequest(activity); or Don't override this method if not in use
            }

            @Override
            public List<String> setPermission() {
                // If You Don't want to check permission automatically and check your own custom permission
                // Use super.setPermission(); or Don't override this method if not in use
                List<String> customPermission = new ArrayList<>();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    customPermission.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                }
                customPermission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                customPermission.add(Manifest.permission.VIBRATE);
                customPermission.add(Manifest.permission.CAMERA);
                return customPermission;
            }
        };

        permission.checkAndRequestPermissions(this);

        initial();
    }

    private void initial() {
        inputUsername = findViewById(R.id.textInputLayoutUsername);
        inputPassword = findViewById(R.id.textInputLayoutPassword);
        edUsername = findViewById(R.id.username);
        edPassword = findViewById(R.id.password);
        CardView btnLogin = findViewById(R.id.btnLogin);
        Button btn_lupa_pw = findViewById(R.id.btn_lupa_pw);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (Objects.requireNonNull(edUsername.getText()).toString().isEmpty()) {
                        inputUsername.setError("Username Kosong");
                        inputPassword.setError(null);
                    } else if (Objects.requireNonNull(edPassword.getText()).toString().isEmpty()) {
                        inputPassword.setError("Password Kosong");
                        inputUsername.setError(null);
                    } else {
                        inputUsername.setError(null);
                        inputPassword.setError(null);
                        postLogin();
                    }
                }
            }
        });

        btn_lupa_pw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                show_form_lupa();
            }
        });
    }

    private void postLogin() {
        M.showLoadingDialog(this);
        Call<M_login> call = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            call = RetrofitClient.getInstance().getApi().userLogin(Objects.requireNonNull(edUsername.getText()).toString().trim(), edPassword.getText().toString().trim());
        }
        assert call != null;
        call.enqueue(new Callback<M_login>() {
            @Override
            public void onResponse(Call<M_login> call, retrofit2.Response<M_login> response) {
                M.hideLoadingDialog();
                Log.e("LOG", String.valueOf(response.body().getResponce()));
                List<M_login_user> res_login = response.body().getLogin();

                Session_management sessionManagement = new Session_management(LoginActivity.this);

                if (res_login.get(0).getStatusLogin().equals("berhasil")) {
                    List<M_login_data> res_data = response.body().getData();
                    sessionManagement.createLoginSession(res_data.get(0));
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();

                } else {
                    Toast.makeText(getApplicationContext(), "User Name dan Password Salah", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<M_login> call, Throwable t) {
                M.hideLoadingDialog();
                Toast.makeText(getApplicationContext(), "Masalah jaringan, coba lagi. ", Toast.LENGTH_SHORT).show();
            }
        });
    }
}