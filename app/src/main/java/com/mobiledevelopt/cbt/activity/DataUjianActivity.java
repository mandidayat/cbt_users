package com.mobiledevelopt.cbt.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mobiledevelopt.cbt.R;
import com.mobiledevelopt.cbt.adapter.ujianAdapter;
import com.mobiledevelopt.cbt.api.Const;
import com.mobiledevelopt.cbt.model.ujian.M_data_ujian;
import com.mobiledevelopt.cbt.model.ujian.M_ujian;
import com.mobiledevelopt.cbt.util.RetrofitClient;
import com.mobiledevelopt.cbt.util.Session_management;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class DataUjianActivity extends AppCompatActivity implements View.OnClickListener {

    private AlertDialog alertDialog;
    private RecyclerView recyclerView;
    private ujianAdapter anggotaAdapter;
    private List<M_data_ujian> list_anggota = new ArrayList<>();
    private LinearLayout ll_error,ll_loading,ll_no_data;
    private Session_management sessionManagement;
    private HashMap<String, String> user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_ujian);

        sessionManagement = new Session_management(DataUjianActivity.this);
        user = sessionManagement.getUserDetails();


//        Toast.makeText(this, user.get(Const.KEY_ID), Toast.LENGTH_SHORT).show();
        initial();

    }

    private void initial() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Session_management sessionManagement = new Session_management(DataUjianActivity.this);
        user = sessionManagement.getUserDetails();

        ll_loading = findViewById(R.id.ll_loading_rv);
        ll_error = findViewById(R.id.ll_error_rv);
        ll_no_data = findViewById(R.id.ll_no_data_rv);
        Button reload = findViewById(R.id.btn_reload_rv);

        recyclerView = findViewById(R.id.recycleVIew);
        list_anggota = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
//                linearLayoutManager.getOrientation());
//        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(anggotaAdapter);

        getUjian();

        reload.setOnClickListener(this);
    }

    private void getUjian() {
        ll_loading.setVisibility(View.VISIBLE);
        ll_error.setVisibility(View.GONE);
        Call<M_ujian> call = null;
        call = RetrofitClient.getInstance().getApi().userUjian(user.get(Const.KEY_NISN));
        call.enqueue(new Callback<M_ujian>() {
            @Override
            public void onResponse(Call<M_ujian> call, retrofit2.Response<M_ujian> response) {
                ll_loading.setVisibility(View.GONE);
                M_ujian anggotaResponse = response.body();

                if (anggotaResponse.getData().size() > 0){
                    recyclerView.setVisibility(View.VISIBLE);
                    list_anggota = anggotaResponse.getData();
                    anggotaAdapter = new ujianAdapter();
                    anggotaAdapter.addAnggota(list_anggota);
                    recyclerView.setAdapter(anggotaAdapter);
                }else{
                    ll_no_data.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<M_ujian> call, Throwable t) {
                Log.e("failur",t.getMessage());
                ll_loading.setVisibility(View.GONE);
                ll_error.setVisibility(View.VISIBLE);
            }
        });
    }


    @Override
    public void onClick(View view) {

    }
}