package com.mobiledevelopt.cbt.model.kuis;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class M_kuis {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<M_kuis_data> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<M_kuis_data> getData() {
        return data;
    }

    public void setData(List<M_kuis_data> data) {
        this.data = data;
    }
}
