package com.mobiledevelopt.cbt.model.ujian;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class M_data_ujian {
    @SerializedName("nisn")
    @Expose
    private String nisn;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("tgl_lahir")
    @Expose
    private String tglLahir;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("ayah")
    @Expose
    private String ayah;
    @SerializedName("ibu")
    @Expose
    private String ibu;
    @SerializedName("wali")
    @Expose
    private String wali;
    @SerializedName("id_ujian")
    @Expose
    private String idUjian;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("nama_ujian")
    @Expose
    private String namaUjian;
    @SerializedName("tgl_ujian")
    @Expose
    private String tglUjian;
    @SerializedName("stat")
    @Expose
    private String stat;
    @SerializedName("ket")
    @Expose
    private String ket;
    @SerializedName("waktu")
    @Expose
    private String waktu;
    @SerializedName("status_ujian")
    @Expose
    private String status_ujian;
    @SerializedName("nilai")
    @Expose
    private String nilai;

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }

    public String getNisn() {
        return nisn;
    }

    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(String tglLahir) {
        this.tglLahir = tglLahir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getAyah() {
        return ayah;
    }

    public void setAyah(String ayah) {
        this.ayah = ayah;
    }

    public String getIbu() {
        return ibu;
    }

    public void setIbu(String ibu) {
        this.ibu = ibu;
    }

    public String getWali() {
        return wali;
    }

    public void setWali(String wali) {
        this.wali = wali;
    }

    public String getIdUjian() {
        return idUjian;
    }

    public void setIdUjian(String idUjian) {
        this.idUjian = idUjian;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNamaUjian() {
        return namaUjian;
    }

    public void setNamaUjian(String namaUjian) {
        this.namaUjian = namaUjian;
    }

    public String getTglUjian() {
        return tglUjian;
    }

    public void setTglUjian(String tglUjian) {
        this.tglUjian = tglUjian;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getStatus_ujian() {
        return status_ujian;
    }

    public void setStatus_ujian(String status_ujian) {
        this.status_ujian = status_ujian;
    }
}
