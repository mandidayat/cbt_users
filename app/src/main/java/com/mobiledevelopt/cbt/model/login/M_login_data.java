package com.mobiledevelopt.cbt.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class M_login_data {
    @SerializedName("id_peserta")
    @Expose
    private String idPeserta;
    @SerializedName("nisn")
    @Expose
    private String nisn;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("tgl_lahir")
    @Expose
    private String tgl_lahir;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("no_hp")
    @Expose
    private String no_hp;
    @SerializedName("ayah")
    @Expose
    private String ayah;
    @SerializedName("ibu")
    @Expose
    private String ibu;
    @SerializedName("wali")
    @Expose
    private String wali;
    @SerializedName("id_ujian")
    @Expose
    private String id_ujian;

    public String getIdPeserta() {
        return idPeserta;
    }

    public void setIdPeserta(String idPeserta) {
        this.idPeserta = idPeserta;
    }

    public String getNisn() {
        return nisn;
    }

    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTgl_lahir() {
        return tgl_lahir;
    }

    public void setTgl_lahir(String tgl_lahir) {
        this.tgl_lahir = tgl_lahir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getAyah() {
        return ayah;
    }

    public void setAyah(String ayah) {
        this.ayah = ayah;
    }

    public String getIbu() {
        return ibu;
    }

    public void setIbu(String ibu) {
        this.ibu = ibu;
    }

    public String getWali() {
        return wali;
    }

    public void setWali(String wali) {
        this.wali = wali;
    }

    public String getId_ujian() {
        return id_ujian;
    }

    public void setId_ujian(String id_ujian) {
        this.id_ujian = id_ujian;
    }
}
