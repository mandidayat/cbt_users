package com.mobiledevelopt.cbt.model.kuis;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class M_kuis_data {
    @SerializedName("id_soal")
    @Expose
    private String idSoal;
    @SerializedName("id_ujian")
    @Expose
    private String idUjian;
    @SerializedName("nama_ujian")
    @Expose
    private String namaUjian;
    @SerializedName("tgl_ujian")
    @Expose
    private String tglUjian;
    @SerializedName("waktu")
    @Expose
    private String waktu;
    @SerializedName("id_mapel")
    @Expose
    private String idMapel;
    @SerializedName("mapel")
    @Expose
    private String mapel;
    @SerializedName("soal")
    @Expose
    private String soal;
    @SerializedName("gambar")
    @Expose
    private String gambar;
    @SerializedName("jawaban")
    @Expose
    private String jawaban;
    @SerializedName("pil1")
    @Expose
    private String pil1;
    @SerializedName("pil2")
    @Expose
    private String pil2;
    @SerializedName("pil3")
    @Expose
    private String pil3;
    @SerializedName("pil4")
    @Expose
    private String pil4;

    public String getIdSoal() {
        return idSoal;
    }

    public void setIdSoal(String idSoal) {
        this.idSoal = idSoal;
    }

    public String getIdUjian() {
        return idUjian;
    }

    public void setIdUjian(String idUjian) {
        this.idUjian = idUjian;
    }

    public String getNamaUjian() {
        return namaUjian;
    }

    public void setNamaUjian(String namaUjian) {
        this.namaUjian = namaUjian;
    }

    public String getTglUjian() {
        return tglUjian;
    }

    public void setTglUjian(String tglUjian) {
        this.tglUjian = tglUjian;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getIdMapel() {
        return idMapel;
    }

    public void setIdMapel(String idMapel) {
        this.idMapel = idMapel;
    }

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }

    public String getSoal() {
        return soal;
    }

    public void setSoal(String soal) {
        this.soal = soal;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getJawaban() {
        return jawaban;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }

    public String getPil1() {
        return pil1;
    }

    public void setPil1(String pil1) {
        this.pil1 = pil1;
    }

    public String getPil2() {
        return pil2;
    }

    public void setPil2(String pil2) {
        this.pil2 = pil2;
    }

    public String getPil3() {
        return pil3;
    }

    public void setPil3(String pil3) {
        this.pil3 = pil3;
    }

    public String getPil4() {
        return pil4;
    }

    public void setPil4(String pil4) {
        this.pil4 = pil4;
    }

}
