package com.mobiledevelopt.cbt.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class M_login {

    @SerializedName("responce")
    @Expose
    private Boolean responce;
    @SerializedName("login")
    @Expose
    private List<M_login_user> login = null;
    @SerializedName("data")
    @Expose
    private List<M_login_data> data = null;

    public Boolean getResponce() {
        return responce;
    }

    public void setResponce(Boolean responce) {
        this.responce = responce;
    }

    public List<M_login_user> getLogin() {
        return login;
    }

    public void setLogin(List<M_login_user> login) {
        this.login = login;
    }

    public List<M_login_data> getData() {
        return data;
    }

    public void setData(List<M_login_data> data) {
        this.data = data;
    }


}
