package com.mobiledevelopt.cbt.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class M_login_user {
    @SerializedName("status_login")
    @Expose
    private String statusLogin;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatusLogin() {
        return statusLogin;
    }

    public void setStatusLogin(String statusLogin) {
        this.statusLogin = statusLogin;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
