package com.mobiledevelopt.cbt.model.ujian;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class M_ujian {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<M_data_ujian> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<M_data_ujian> getData() {
        return data;
    }

    public void setData(List<M_data_ujian> data) {
        this.data = data;
    }
}
