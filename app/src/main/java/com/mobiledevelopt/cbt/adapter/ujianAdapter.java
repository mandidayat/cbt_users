package com.mobiledevelopt.cbt.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.mobiledevelopt.cbt.R;
import com.mobiledevelopt.cbt.activity.TestActivity;
import com.mobiledevelopt.cbt.model.ujian.M_data_ujian;
import com.mobiledevelopt.cbt.util.M;

import java.util.ArrayList;
import java.util.List;

public class ujianAdapter extends RecyclerView.Adapter<ujianAdapter.MyViewHolder> {

    private List<M_data_ujian> layanan;
    private Context context;
    AlertDialog alertDialog;

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_nama,tv_id,tv_ket,tv_tgl,tv_waktu;
        private Button btn_test,btn_selesai;

        MyViewHolder(View view) {
            super(view);
            context = view.getContext();
            tv_nama = view.findViewById(R.id.tv_nama_ujian);
            tv_id = view.findViewById(R.id.tv_id_ujian);
            tv_tgl = view.findViewById(R.id.tv_tgl);
            tv_ket = view.findViewById(R.id.tv_ket);
            tv_waktu = view.findViewById(R.id.tv_waktu);

            btn_test = view.findViewById(R.id.btn_test);
            btn_selesai = view.findViewById(R.id.btn_selesai);
        }
    }

    public void addAnggota(List<M_data_ujian> layanan) {
        this.layanan = layanan;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ujian, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final M_data_ujian model = layanan.get(position);

        holder.tv_tgl.setText(M.convertDate(model.getTglUjian()));
        holder.tv_nama.setText(model.getNamaUjian());
        holder.tv_id.setText(model.getIdUjian());
        holder.tv_ket.setText(model.getKet());
        holder.tv_waktu.setText(model.getWaktu() + " menit");
        if (model.getStatus_ujian().equals("1")){
            holder.btn_test.setVisibility(View.GONE);
            holder.btn_selesai.setVisibility(View.VISIBLE);
        }else {
            holder.btn_test.setVisibility(View.VISIBLE);
            holder.btn_selesai.setVisibility(View.GONE);
        }
        holder.btn_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TestActivity.class);
                intent.putExtra("waktu",model.getWaktu());
                intent.putExtra("id_ujian",model.getIdUjian());
                context.startActivity(intent);
            }
        });

        holder.btn_selesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show_nilai(model.getNilai());
            }
        });

    }

    @Override
    public int getItemCount() {
        return layanan.size();
    }

    public void setFilter(ArrayList<M_data_ujian> filterList){
        layanan = new ArrayList<>();
        layanan.addAll(filterList);
        notifyDataSetChanged();
    }

    private void show_nilai(String nilai) {
        LayoutInflater mInflater = LayoutInflater.from(context);
        View v = mInflater.inflate(R.layout.hasil_ujian_layout, null);
        final AlertDialog dialog = new AlertDialog.Builder(context).create();
        final TextView tv = v.findViewById(R.id.tv_hasil_ujian);
        tv.setText("Anda Mendapat Nilai : \n " + nilai);
        dialog.setView(v);

        dialog.show();
    }
}
